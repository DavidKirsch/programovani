peníze=20000
náboje=240
granáty=4
HP=435
presnost=0.50
poskozeni=50
inventar=[]
shturman = 0
gluhar = 0
dealmaker = 0
killa = 0
import time
import random


def start():
    print("Pro zpuštění hry zadejte 1")
    zpusteni = int(input(" "))
    if (zpusteni == 1):
        ukol1()
    elif(zpusteni == 2):
        prapor()
    else:
        print("Napsal jste to blbě")
        start()


def ukol1():
    print("Náhle se probudíš, nevíš kde si, jsi dezorientovaný.")
    time.sleep(1)
    print("Naposled si pamatuješ jak letíš vrtulníkem na předsunutou základnu v Cherrnu")
    time.sleep(1)
    print("Asi deset metrů po tvé pravé ruce vidíš jen trosky vrtulníku a mrtvoly pilotů a člennů tvého týmu")
    time.sleep(1)
    print("U sebe máš pouze svou zbraň a tři zásobníky")
    time.sleep(1)
    print("Co uděláš?")
    print("1 = Prouzkoumat vrak vrtulníku")
    print("2 = Jít pryč")

    rozhodnuti1 = input(" ")

    if (rozhodnuti1 == "1"):
        vrtulnik()
    elif (rozhodnuti1 == "2"):
        cesta()
    else:
        print("Zadal jste to špatně")
        ukol1()

def vrtulnik():
    print("Přijdeš blíž k vrtulníku.")
    print("Vrtulník je na dvě části.")
    time.sleep(1)
    print("Vypadá to jako by ho trefila raketa.")
    time.sleep(1)
    print("Co uděláš?")
    print("1 = Zkontroluješ jestli někdo nepřežil")
    print("2 = Zkusíš poslat SOS rádiem z kokpitu")
    print("3 = Radši utečeš protože museli vědět kam ten vrtulník spadnul")

    rozhodnutivrtulnik = input(" ")

    if (rozhodnutivrtulnik == "1"):
        print("Zkontroluješ tep všech 5 člennů tvého týmu a 2 pilotů")
        time.sleep(3)
        print("Bohůžel to nikdo nepřežil")
        cesta()
    elif (rozhodnutivrtulnik == "2"):
        print("Vlezeš do kokpitu najdeš vysílačku")
        print("Do rádia řekneš MONOLIT Chernno tady skif SOS vrtulník zasažen raketou")
        time.sleep(1)
        print("Z rádia se ozve pouze šum")
        print("Rozhodneš se radši odejít")
        cesta()
    elif (rozhodnutivrtulnik == 3):
        print("Někdo určitě půjde prozkoumat trosky vrtulníku který sestřelil")
        print("Raději odejdeš")
        cesta()
    else:
        print("Napsal jste to blbě")
        rozhodnutivrtulnik()


def cesta():
    print("Pomale jdeš vedle cesty.")
    print("Uvidíš rozcestník")
    time.sleep(1)
    print("Cherrno 15km")
    print("Oddechneš si a pomale se vydáš směrem k základně Cherrno")
    Chernno()


def Chernno():
    print("Při příchodu k základně jsi dal pušku na záda a dal ruce na hlavu a pomale si šel k základně")
    time.sleep(1)
    print("Po chvíli se ze základny ozvalo KDO JSME")
    print("Ty jsi odpověděl ZHOUBA NAŠICH NEPŘÁTEL")
    time.sleep(1)
    print("Poté se otevřely vrata od základny")
    print("Vešel si dovnitř")
    zakladna()



def zakladna():
    print("Po příchodu dovnitř tě pozdraví voják")
    print("Ahoj, měl by si jít za velitelem")
    time.sleep(1)
    print("Šel si za velitelem")
    print("Zaklepal si na dveře ze kterých se pak ozvalo Vstupte")
    time.sleep(1)
    print("Po vstoupení dovnitř si se představil a vysvětlil si co se stalo")
    print("Pak tě poslal za Praporem vedoucím skladníkem")
    print("Než si zavřel dveře tak ti řekl ať pak přijdeš do kanceláře V.")
    prapor()
def prapor():
    print("Po příchodu ti prapor řekl : Když mi přineseš dost peněz tak ti můžu sehnat prakticky cokoliv.")
    time.sleep(1)
    print("Třeba když si koupíš zaměřovač zvýší se ti přesnost, nebo když si koupíš lepší náboje zvýší se ti poškození")
    print("Pak až si nakoupíš věci tka přijď do kanceláře číslo 5")

    rozhodnutiprapor()




def rozhodnutiprapor():
    print("Chceš si něco koupit. Pokud ano zadej 1 pokud ne tak 2")
    obchodovat = input("Chceš si něco koupit?")
    if (obchodovat == "1"):
        obchod()
    elif (obchodovat == "2"):
        kancelar5()
    else:
        print("Napsal jste to blbě")
        rozhodnutiprapor()



def obchod():
    global peníze
    print("Můžeš si koupit")
    print("Máš", peníze, "peněz" )
    print("Zaměřovač ACOG + 20 přesnost, stojí 8000 peněz")
    print("Lepší náboje + 10 poškození, stojí 5000 peněz")
    print("Laser + 10 přesnost, stojí 3000 peněz")
    print("Svazek klíčů s logem Cobalt industries, stojí 6000 peněz")
    kupovat()

def kupovat():
    print("1 = Zaměřovač")
    print("2 = Náboje")
    print("3 = Laser")
    print("4 = Klíče")
    volbakupovat()




def volbakupovat():
    global peníze
    global presnost
    global poskozeni
    global inventar
    cokupovat = input("Zadejte číslo předmětu který si chcete koupit")
    if (cokupovat == "1"):
        presnost = presnost + 0.20
        peníze = peníze - 8000
        print("Chcete koupit ještě něco? pokud ano napište 1 pokud ne tak 2")
        kupovatanone = input()
        if (kupovatanone == 1):
            kupovat()
        elif (kupovatanone ==2):
            kancelar5()
        else:
            print("Napsal jste to blbě")
            volbakupovat()
    elif (cokupovat == "2"):
        poskozeni = poskozeni + 10
        peníze = peníze - 5000
        print("Chcete koupit ještě něco? pokud ano napište 1 pokud ne tak 2")
        kupovatanone = input()

        if (kupovatanone == 1):
            kupovat()
        elif (kupovatanone ==2):
            kancelar5()
        else:
            print("Napsal jste to blbě")
            volbakupovat()
    elif (cokupovat == "3"):
        presnost = presnost + 0.10
        peníze = peníze - 3000
        print("Chcete koupit ještě něco? pokud ano napište 1 pokud ne tak 2")
        kupovatanone = input()
        if (kupovatanone == 1):
            kupovat()
        elif (kupovatanone ==2):
            kancelar5()
        else:
            print("Napsal jste to blbě")
            volbakupovat()
    elif (cokupovat == "4"):
        dejdoinventare("klice")
        print("Chcete koupit ještě něco? pokud ano napište 1 pokud ne tak 2")
        kupovatanone = input()
        if (kupovatanone == 1):
            kupovat()
        elif (kupovatanone ==2):
            kancelar5()
        else:
            print("Napsal jste to blbě")
            volbakupovat()
    else:
        print("Zadal jste to špatně")

def dejdoinventare(co):
    inventar.append(co)
    print(inventar)
    kancelar5()

def kancelar5():
    global gluhar
    global shturman
    global dealmaker
    global killa
    print("Po zaklepání na dveře si vstoupil dovnitř")
    print("V kanceláři bylo 5 mužů")
    print("Zdravím Skife můžete si vybrat nové kolegy, ale má to háček můžete si vybrat jen dva")
    time.sleep(1)
    print("Můžeš si vybrat mezi")
    print("Gluharem který je dobrý s elektronikou,(1)")
    time.sleep(1)
    print("Shturmanem který dokáže vypáčit zámek za zlomek sekundy,(2)")
    time.sleep(1)
    print("Dealmakerem který se dokáže vykecat i z vraždy,(3)")
    time.sleep(1)
    print("A Killou který dokáže porazit kohokoliv v bitvě(4)")
    kterykolega = input("Vyberte si podle čísel v závorkách a nedávejte jednoho dvakrát to to rozbije")
    pocetkolegu = 0
    while (pocetkolegu != 2):
        if (kterykolega == 1):
            gluhar = gluhar + 1
            pocetkolegu = pocetkolegu + 1
        elif (kterykolega == 2):
            shturman = shturman + 1
            pocetkolegu = pocetkolegu + 1
        elif (kterykolega == 3):
            dealmaker = dealmaker + 1
            pocetkolegu = pocetkolegu + 1
        elif (kterykolega == 4):
            killa = killa + 1
            pocetkolegu = pocetkolegu + 1
        else:
            print("Napsal jste to blbě")
    mise2()
#mise2()  Coming in červen
start()






start()
#Zatím vše
